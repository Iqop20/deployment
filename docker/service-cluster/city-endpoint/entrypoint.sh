#!/bin/bash
mkdir -p /opt/authzforce-ce-server/data/domains/
sleep $[ ( $RANDOM % 15 )  + 1 ]s
if [ "$(ls -A /opt/authzforce-ce-server/conf)" ]; then
    echo "/opt/authzforce-ce-server/conf already contains data, not writing"
else
    cd /
    tar -xvf /authz-conf.tar.gz 
fi
/usr/bin/contextBroker -fg -multiservice -dbhost ${E_MONGO} -rplSet ${E_RS} -noCache &
cd /usr/share/tomcat9 && ./bin/catalina.sh run &
cd /
java -cp /inconnu.jar inconnu.crl.CRLServer ${E_mongoUrl} &
echo "Waiting 30 seconds to start the endpoint"
sleep 30
java -cp /inconnu.jar inconnu.ptasc.bootstrappers.EndpointBootstrapper ${E_CITYECIESPUB} ${E_CITYECDSAPUB} 0 ${E_CITYDOMAIN} ${E_ORION} ${E_AUTHZ} ${E_CRLIP}
