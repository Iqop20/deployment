#!/bin/bash
mkdir -p /opt/authzforce-ce-server/data/domains/
sleep $[ ( $RANDOM % 15 )  + 1 ]s
if [ "$(ls -A /opt/authzforce-ce-server/conf)" ]; then
    echo "/opt/authzforce-ce-server/conf already contains data, not writing"
else
    cd /
    tar -xvf /authz-conf.tar.gz 
fi
cd /usr/share/tomcat9 && ./bin/catalina.sh run &
cd /
java -cp /inconnu.jar inconnu.crl.CRLServer ${E_mongoUrl} &
echo "Waiting 30 seconds to start the manager"
sleep 30
java -cp /inconnu.jar inconnu.ptasc.bootstrappers.CityBootstrapper ${E_authzURL} ${E_cityDomain} ${E_crlIP} ${E_yubikeyPublicId} ${E_yubikeySecret} 