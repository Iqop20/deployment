#!/bin/bash

/usr/bin/contextBroker -fg -multiservice -dbhost ${E_MONGO} -rplSet ${E_RS} -noCache &
java -jar /off-prem.jar ${E_ORION_IP} ${E_mongoURL} ${E_storagePath} clouds.cfg ${E_IP}
