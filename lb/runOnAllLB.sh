#!/bin/bash
sudo add-apt-repository ppa:vbernat/haproxy-2.2
sudo apt update
sudo apt install haproxy psmisc -y
sudo cp haproxy.cfg /etc/haproxy/
sudo systemctl restart haproxy
sudo systemctl enable haproxy

# echo "Is slave or master? "
# read s

# if [[ "$s" == "master" ]] ; 
# then 
#     sudo cp keepalived-master.conf /etc/keepalived/keepalived.conf
# else
#     sudo cp keepalived-slave.conf /etc/keepalived/keepalived.conf
# fi

# echo "Change the commented section on the /etc/keepalived/keepalived.conf on both master and slave"
