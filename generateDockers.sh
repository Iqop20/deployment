#!/bin/bash

if [[ ( $# != 5 )]]
then
    echo "Needed: first? cityDomain yubikeyPublicId yubikeySecret dockerID"
    exit
fi

cityDomain=$2
yubikeyPublicId=$3
yubikeySecret=$4
dockerID=$5


if [[ "$1" == "true" ]] ; 
then
    sudo apt-get update
    sudo apt install maven openjdk-11-jdk git
    cd ../anonymization-core && mvn clean install && cd .. || exit
    cd crl && mvn clean install && cd .. || exit
    cd ptasc && mvn clean package || exit
    mv target/ptasc-1.2.jar ../deployment/inconnu.jar 
    cd ../deployment
fi

echo "Building base image"
cp inconnu.jar docker/base 
docker build --no-cache -t inconnu/base docker/base || exit

echo "Building manager keys"
mkdir managerK
cd managerK
java -cp ../inconnu.jar inconnu.ptasc.bootstrappers.GenerateOnlyCityManagerKeys $cityDomain
rm -rf ../docker/service-cluster/city-manager/keys
mkdir ../docker/service-cluster/city-manager/keys
cp *.ptasc ../docker/service-cluster/city-manager/keys
cd ..

echo "Building endpoint keys"
mkdir endpointK
cd endpointK
java -cp ../inconnu.jar inconnu.ptasc.bootstrappers.GenerateEndpointKeys $cityDomain $( cat ../managerK/CityprivKey.ptasc | base64 -w0 ) $( cat ../managerK/CityCertificate.ptasc | base64 -w0 )
rm -rf ../docker/service-cluster/city-endpoint/keys
mkdir ../docker/service-cluster/city-endpoint/keys
cp *.ptasc ../docker/service-cluster/city-endpoint/keys
cd ..

mkdir CITY_PUBLIC
cd CITY_PUBLIC
mv ../managerK/CityQR.png .
mv ../managerK/ECDSAb64.ptasc .
mv ../managerK/ECIESb64.ptasc .
cd ..

rm -rf managerK
rm -rf endpointK

echo "Manager Public Keys on CITY_PUBLIC folder"

echo "Building endpoint image"
docker build --no-cache --build-arg cityDomain=${cityDomain} --build-arg cityECIESpub=$( cat CITY_PUBLIC/ECIESb64.ptasc ) --build-arg cityECDSApub=$( cat CITY_PUBLIC/ECDSAb64.ptasc ) -t inconnu/city-endpoint docker/service-cluster/city-endpoint  || exit

echo "Building manager image"
docker build --no-cache --build-arg yubikeyPublicId=${yubikeyPublicId} --build-arg yubikeySecret=${yubikeySecret} --build-arg cityDomain=${cityDomain} -t inconnu/city-manager docker/service-cluster/city-manager || exit

echo "Building Off-premise image"
docker build --no-cache -t inconnu/off-prem docker/off-prem

echo "Publishing images"
docker login
docker tag inconnu/city-endpoint $dockerID/city-endpoint
docker push $dockerID/city-endpoint

docker tag inconnu/city-manager $dockerID/city-manager
docker push $dockerID/city-manager

docker tag inconnu/off-prem $dockerID/off-prem
docker push $dockerID/off-prem

docker build -t inconnu/mongo0 docker/mongo/mongo-r0
docker tag inconnu/mongo0 $dockerID/mongo0
docker push $dockerID/mongo0

docker build -t inconnu/mongo1 docker/mongo/mongo-r1
docker tag inconnu/mongo1 $dockerID/mongo1
docker push $dockerID/mongo1

docker build -t inconnu/mongo2 docker/mongo/mongo-r2
docker tag inconnu/mongo2 $dockerID/mongo2
docker push $dockerID/mongo2


