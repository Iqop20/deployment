#!/bin/bash

sudo kubeadm --config kubeadm-config.yaml init
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
kubectl apply -f https://docs.projectcalico.org/archive/v3.19/manifests/calico.yaml 

