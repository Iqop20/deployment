#!/bin/bash


echo "Creating City Storage"
echo "Have you a RAW storage disk or partition on all nodes, it is necessary to continue..."
read 

kubectl create -f city-storage/crds.yaml -f city-storage/common.yaml -f city-storage/operator.yaml
kubectl create -f city-storage/cluster.yaml
kubectl apply -f city-storage/toolbox.yaml
echo "Please wait for osds to appear in order to active the remainder of the city-infrastructure"
read

kubectl apply -f city-storage/filesystem.yaml
kubectl apply -f city-storage/storage-class.yaml

echo "Please wait for myfs to appear in order to active the remainder of the city-infrastructure"
read

echo "Creating City DB"

kubectl apply -f city-db/city-db-ns.yaml
kubectl apply -f city-db/ceph-db-pvc.yaml
kubectl apply -f city-db/mongo.yaml
echo "Please wait for 3 dbs to start"
read
echo "Sleeping 60 seconds"
sleep 60
kubectl exec -it $( kubectl get pods -l app=mongo-r0 -n city-db -o jsonpath='{.items[0].metadata.name}' ) -n city-db -- /bin/bash -c "mongo < /comm.txt" || exit

echo "Creating off-premise controller"
kubectl apply -f off-prem/off-premise.yaml
echo "Creating swift storage instances"
kubectl apply -f off-prem/openstack-storage.yaml
echo "Wait until 3 swift instances and the off-premise controller are started"
read
kubectl exec -it $( kubectl get pods -l app=controller -n off-prem -o jsonpath='{.items[0].metadata.name}' ) -n off-prem -- /bin/bash -c "sh /initSwift.sh"


echo "Creating City Front"
kubectl apply -f city-front/city-front-ns.yaml
kubectl apply -f city-front/ceph-city-authz-conf-pvc.yaml
kubectl apply -f city-front/ceph-city-authz-data-pvc.yaml
kubectl apply -f city-front/manager.yaml
kubectl apply -f city-front/endpoint.yaml

echo "Wait for the all the city front to start to provision devices"
read